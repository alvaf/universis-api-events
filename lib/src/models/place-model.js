import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {Array<Event|any>} events
 * @property {string} globalLocationNumber
 * @property {number} maximumAttendeeCapacity
 * @property {string} map
 * @property {string} branchCode
 * @property {PostalAddress|any} address
 * @property {string} logo
 * @property {string} telephone
 * @property {GeoCoordinates|any} geo
 * @property {Place} containedInPlace
 * @property {boolean} publicAccess
 * @property {string} faxNumber
 * @property {boolean} isAccessibleForFree
 * @augments {DataObject}
 */
@EdmMapping.entityType('Place')
class Place extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Place;
